<!-- TITLE: Builds à set Mintarn-->
<!-- SUBTITLE: Des builds pour les nouveaux -->


# Module pour tester les builds

Une version du Vordan's Hero Creator modifiée pour utiliser les ressources de LCDA est disponible <a href="https://lcda-nwn2.fr/lcdavhc/Vordan Hero Lcda.7z">ici</a>

Procédure d'installation:
1. Dézipper les fichiers
2. Installer le .mod dans C:\Mes Documents\Neverwinter Nights 2\modules
3. Installer le .hak dans C:\Mes Documents\Neverwinter Nights 2\hak
4. Lancer le module en local et bon test !
		
# Exemples de builds

## Barbare
* [Le Barbare Tank](http://nwn2db.com/build/?227226) ( Barbare22 / Guerrier4 / NeufPadhiver4 )
## Barde
* [Le Barde Soutien](http://nwn2db.com/build/?191793) ( Barde21 / Hexblade4 / AccordDissonant5 )
## Chaman Spiritiste
* [Le chaman secouriste (CA)](http://nwn2db.com/build/?268123)
* [Le chaman secouriste (DD)](http://nwn2db.com/build/?245388)
* [Le chaman Casteur-Tank](http://nwn2db.com/build/?200866) ( Chamane26 / ChevalierNoir4 )
## Druide

## Éclaireur
* [L'Eclaireur Archer](http://nwn2db.com/build/?172816) ( Eclaireur21 / ArcherElementaire5 / Guerrier4 )
 
## Élu Divin
* [L'Elu Divin Féérique Puissance Divine](http://nwn2db.com/build/?268116) ( EluDivin26 / ChevalierNoir4 )
 
## Ensorceleur
* [L'Ensorceleur Noir](http://nwn2db.com/build/?264312) (Ensorceleur20 / EruditChateausuif6 / ChevalierNoir4 )

## Guerrier
* [Le guerrier frénétique](http://nwn2db.com/build/?188463)
* [Le guerrier de l'onction](http://nwn2db.com/build/?268115)
* [Le guerrier noir](http://nwn2db.com/build/?268430)

## Hexblade
* [Le maudit](http://nwn2db.com/build/?207761)
* [L'Hexblade Puissance divine](http://nwn2db.com/build/?268112)( Hexblade20 / Barde6 / ChevalierNoir4 )

## Magicien
* [Le Magicien Nécromant Rouge](http://nwn2db.com/build/?264575) ( Magicien20 / MageRouge10 )

## Malfrat
* [Le Malfrat Puissance Divine](http://nwn2db.com/build/?152297) ( Malfrat20 / Barde6 / ChevalierNoir4 ) !!!NÉCESSITE LE TOMAHAWK!!!

## Moine
* [Le moine secouriste](http://nwn2db.com/build/?243450)
* [Le Moine Double-Kama](http://nwn2db.com/build/?256142) ( Moine20 / Spadassin5 / SoldatElite5 )

## Ninja
* [Le Ninja Kama-Dague](http://nwn2db.com/build/?168237) ( Ninja20 / Spadassin5 / SoldatElite5 )

## Paladin
* [Le Paladin Puissance Divine](http://nwn2db.com/build/?196791) ( Paladin20 / ChampionFlammeArgent10 )

## Prêtre
* [Le Pretre Puissance Divine](http://nwn2db.com/build/?268120) ( Prêtre20 / Paladin4 / LameEtincelante6 )

## Rôdeur
* [Le Rodeur Archer](http://nwn2db.com/build/?163350) ( Rodeur21 / Guerrier4 / ArcherElementaire5 )

## Roublard
* [Le Roublard Double-Dague](http://nwn2db.com/build/?256063) ( Roublard20 / Spadassin5 / SoldatElite5 )  !!!REMPLACER SLIPPERY MIND PAR DISCRETION TOTALE!!!

## Sorcier
* [Le Sorcier standard](http://nwn2db.com/build/?194192) ( Sorcier30 )

## Spadassin
* [Le spadassassin](http://nwn2db.com/build/?244420)
* [Le Spadassin Double-Rapière](http://nwn2db.com/build/?167019) ( Spadassin20 / Guerrier4 / ChampionIndomptable6 )

