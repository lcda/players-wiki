<!-- TITLE: Accueil -->

<p style="text-align: center; font-size: 4em">
	La Colère d'Aurile<br/>
	Wiki des joueurs
</p>

# __Règles__
1. Les énigmes doivent rester des énigmes: Ne donnez pas les solutions
1. Les [secrets](http://lacoleredaurile.xooit.fr/t334-Liste-des-Quetes.htm) doivent rester secrets: N'indiquez pas clairement la marche à suivre pour débloquer un secret, restez très flou ou mieux n'en parlez pas.

# __Sommaire__
- [Les Choses à savoir](https://wiki.lcda-nwn2.fr/les-choses-a-savoir)
- [Les Set de Mintarn](https://wiki.lcda-nwn2.fr/set-mintarn)
- [Quelques builds](https://wiki.lcda-nwn2.fr/builds)
- [Les Donjons du serveur](https://wiki.lcda-nwn2.fr/all) : Cliquez sur le dossier Donjon pour acceder à tous les donjons du serveur
- [Hagbe](https://wiki.lcda-nwn2.fr/hagbe) : Le forgeron légendaire capable d'améliorer vos objets magiques

# Idées en vrac
- Donjons : En cours
- Classes
	- Description KCP
	- Modifications des classes de bases sur LCDA
- Astuces pour builder son personnages : En cours
- Artisanat
- Background/lore du serveur / villes / zones importantes