<!-- TITLE: Les Choses A Savoir -->
<!-- SUBTITLE: tour d'horizon des spécificités du module -->

# Les HAK
Ce serveur tourne avec des hak particuliers, nottemment le KCP (ou Kaedrin class pack) qui regroupe un ajout conséquent en races, classes , classes de rpestige, dons et sorts. L'équipe de LaColèreD'Aurile a elle aussi travaillé à des ajouts sous forme de sorts.
# L'aventure à plusieurs
Ce serveur encourage fortement le fait de jouer en groupe. En effet, vous progresserez bien plus facilement et rapidement si vous êtes accompagné d'un (ou plus) autre personnage de niveau égal. Hélas, plus l'écart de niveau est grand, moins ce sera avantageux.

* N'hésitez pas à saluer les autres joueurs en vous connectant et à grouper les arrivants.
* De même n'hésitez pas à demander de l'aide pour réaliser un donjon, la plupart des joueurs seront ravis de vous accompagner.
* Plus vous progresserez , et plus le fait de grouper deviendra nécessaire pour avoir une chance.
# Les quêtes
Il existe profusion de quêtes sur ce serveur, des plus simples au plus difficiles , des plus évidentes aux plus secrètes.
La majorité permet d'empocher de l'or et de l'expérience, mais certaines débloquent des chainages de quêtes, des acces à certaines zones, des objets inédits ...
N'hésitez pas à parler à chaque PNJ et à prendre toutes les quêtes possibles.
## Quête de la pierre de rappel
Ceci est la première quête que vous obtiendrez. Elle vous permettra d'obtenir une pierre qui , lorsque'activée, vous téléportera dans l'éther. Depuis l'éther vous aurez acces aux différetns portails que vous aurez débloqué et pourrez vous rendre plus directement aux différents endroits du module.
## Quête chaînées : 
Lorsque l'on termine une quête , bien souvent il est intéressant de reparler aussitot au PNJ qui a toutes les chances de vous proposer une nouvelle quête. 
## Quêtes de collecte :
Certaines quêtes consistent dans le fait de ramener des fragments de créatures. Livrer ces fragments permet d'empocher une récompense en or et en expérience qui sera calculée sur le nombre de ces éléments rapportés et sur l'adéquation entre le niveau de la quête et le niveau du personnage (plus ces niveaux sont proches , plus la récompense sera grande). A savoir qu'il est possible de livrer ces collectes en plusieurs fois mais cependant les récompenses en expérience de ces quetes sont plafonnées; au dela de ce plafond la collecte ne rapportera plus que de l'or.
## Quêtes secrètes :
Certaines quêtes sont cachées et ne se révèlent qu'une fois découvertes ou accomplies. Gardez une part de mystère pour vos compagnons de jeu et évitez le spoil, ces quêtes sont secrètes à dessein. Le plaisir de les découvrir par soi même est gratifiant.
## Zone de farming xp 
  Pour ceux qui préfère tuer des monstres . ( Guide par Dunaria)
1.   Suivre cette voie n'est pas optimale pour gagner de l'xp RAPIDEMENT. Faire les quêtes est toujours préférable.
2.   Jouer à plusieurs max 4 niv de différence.  Idéale 1-2 niveau d'écart. ( Bonus d'xp de groupe)
3.   Xp dans les dungeons en groupe est toujours mieux que d'xp  à l' extérieurs, car la densité des monstres est supérieure.
4.   Tout dépendant de votre équipement et la classe que vous jouer ,  votre  transition sera plus rapide. 

|Niveau | Montres| Zone | Requis|
|---|---|---|---|
| 1-2| Rats | Égouts de la ville| |
| 2-4| Sangliers| | |
| 4-6| Loups | Lurkwood | Immu: Renversement contre le chef de la meute seulement|
| 6-8| Orc | Lurkwood(Coeur de la forêt) -> Lurkwood - Chemin -->**Vers le fortin orc **| Immu: mental / Renversement |
| 8-10| Mort Vivant| Cimetière|( Avoir Immu : maladie/ poison) Sinon achetez vous des CERISES :)|
|10-12| Lézard| Marais | ( Immu: Renversement|
|12-14| Araignée| | Immu : Poison/absorbtion de niveau|
|14-16| Fées | Désolation| Immu: Mental + viosn de l'invisible ou vision lucide|
|16-18| Pillards, Serpent, Araigné |Désert | Immu: Renversement / absorbtion de niveau / liberté de mouvement|
|18-22| Lézard et lutin de feu | Volcan | Immu: Dégats de feu / aborbtion de niveau|
|22-26| Loups de glace |Ile de Glace | Req : Immu : renversement  + Vetement chaud pour dormir)/ 55+ AC / Regen +15 et un truc pour Puller à distance|
|26-28| Géants | Le Glacier | Immu: Renversement / 60+ AC / Regen +15 et un truc pour Puller à distance |
|28-30| Géants | Donjon des géants normal| Immu: Renversement / 65+ AC / Regen +15 et un truc pour Puller à distance|



# Les Kinders
Chaque donjon du module se voit terminé par un affrontement contre un Boss qui disposera de capacités spéciales uniques. Une fois ce boss vaincu, il sera possible de fouiller son butin (il s'agit souvent d'un coffre mais pas toujours). 
Lors de la première fois ou vous tuez ce Boss vous serez gratifié d'un "Kinder": un objet particulier tiré au hasard sur une liste d'une vingtaine d'objets spécifique à ce Boss.
Les fois suivantes, vous aurez 90% de chances de n'obtenir que de l'or et 10% de chances de re-obtenir un Kinder sur la liste.
# Les modes de difficulté
Les donjons haut niveau sont équipés d'un système de difficulté. 
Il existe en tout, 5 modes de difficulté :

* Normal (blanc)
* Difficile (cyan)
* Epique (jaune)
* Légendaire (orange)
* Inimaginable (rouge)

Tous les donjons ne sont pas forcément nuancés de ces 5 modes , certain n'en possèdent qu'un.
Dans chacun de ces modes de difficulté, la puissance des Kinder est en rapport à la puissance des ennemis.

La première fois ou vous terminez un donjon en mode "normal" vous débloquerez l'acces au mode de difficulté supérieur de ce donjon (le mode "difficile" en l'occurence) et il en ira de même pour débloquer chaque mode, un par un, jusqu'au dernier.

Il vous est tout de même possible de participer en groupe à un donjon en cours dont vous n'avez pas encore débloqué la difficulté. Vous bénéficierez du Kinder de votre pallier personnel et du déblocage de votre prochain pallier. 
Exemple : vous n'avez débloqué que le mode difficile d'un donjon et vous participer à ce donjon en mode Légendaire en accompagnant d'autres joueurs, finir ce donjon vous débloquera l'acces à votre prochain palier (le mode Epique) et vous obtiendrez un Kinder de rang Difficile (cyan).
# Les sets de classe
A très haut niveau, il existe un donjon particulier permettant d'obtenir un set de classe au choix. 
Ces sets sont réservés aux personnage disposant d'au moins 20 niveaux dans une classe de base précise et il est impossible de les équiper tant que ce prérequis n'est pas atteint. 
Chaque classe de base dispose d'un set. (c'est à dire les classes qui ne sont pas "de prestige") 

Ces sets sont (à une exception pret) composés de:
* une armure
* une paire de bottes
* une paire de gants
* un casque. 

Ils sont très fortement orientés vers l'archétype standard de la classe en question.
Ils disposent de nombreux bonus divers, de sorts spécifiques, et d'une liste de dons conséquente.

De la même manière que les Kinder, ces sets disposent de versions "difficile" et "epique".
* La version difficile (cyan) dispose d'une pièce supplémentaire : la cape
* La version epique (jaune) dispose d'une pièce au chhoix supplémentaire : une arme au choix parmi 2 ou 3 proposées
# La banque & l'hotel des ventes
Le module dispose de puissants outils de stockage et de triage : Chez Ibée, le banquier.
Une fois payés les frais d'inscription , vous aurez acces à 
* une banque de dépot monétaire qui sera commune à tous vos personnages
* une banque de dépot d'objets qui sera elle aussi commune à tous vos personnages. Le nombre d'objets stockable est extensible, en payant (l'icone du petit +)
* un hotel des ventes qui sera consultable et utilisable par tous les joueurs. Vous pourrez y mettre en vente vos butins ou acheter ceux que d'autres auront mis en vente.

# Atlas
--- projet GPS, pour ceux qui se perdent toujours..