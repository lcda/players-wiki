<!-- TITLE: Set Mintarn -->
<!-- SUBTITLE: Les sets Mintarn et leurs stats, pour éviter de devoir refaire son build -->

NOTE:  Seul le set Blanc Barbare est correct.  Le tableau a été copier partout. Reste juste à éditer le contenu. Note  respecter le nombre de charactère  remplacer par colonne sinon le tableau 
va faire une erreur.  Une personne à la fois peux éditer. Sinon l'un de vous vera son travail éffacer pcq l'autre a sauvegardé après vous et ce sans vos modifications.
*          N = NON FAIT
*          OK = Correct et révisé
*          Partiel = Partiellement remplis
NOTE 2: Le Sommaire BUG ( les choix à gauche ) si vous persister à mettre  ## blanc  ##bleu etc..  on ne peux pas scroll vers le bas pour voir le reste des classes. 
Seul la classe reste en sommaire. La liste entre dans une page.

# Barbare ( blanc OK,  Bleu N, JAUNE Partiel: manque 2 choix d'armes)
Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |
|                           |                          |                                  |Immunité contondant +5%                                 |
|                           |                          |                                  |Immunité perforant +5%                                  |
|                           |                          |                                  |Immunité tranchant +5%                                  |
|                           |                          |                                  |Points de vie +50                                       |
|                           |                          |                                  |DR 6/Métal (fer)                                        |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |

Bleu
 
| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |
|                           |                          |                                  |Points de vie +50                                       |                             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |

 Jaune


| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |     Choix d'armes #1  |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +25          |Constitution 12            |Dextérité 6                        |Force 8                                                 | Don Combat en aveugle                            |** Grande Hache Sanglante de Grakar   ** |
| Perception auditive +20   |CA 10 (Armure)             |CA 7 (esquive)                     |Don Arme de prédilection sup. (grande hache)            | Immunité Acide 10%                            |Grande Hache             |
| Raillerie +25             |Pierre ioun constit +1     |Don Maniement des armes exotiques  |Don Arme de prédilection sup. (hache d'arme)            | Immunité contondant +5%                            |Au toucher Sanglante 3             |
| CA 8 (Parade)             |Pierre ioun force +1       |Don Vélocité                       |Don Arme de prédilection sup. (hache de guerre naine)   | Immunité Electricité 10%                            |Bonus dégâts: Contondant 1d12             |
| Don Odorat                |Immunité contondant +15%   |Immunité renversement              |Don Arme de prédilection sup. (hache de lancer)         | Immunité energie Négative 10%                            |Altération +7             |
| Don Rages supplémentaires |Immunité perforant +15%    |Immunité contondant +5%            |Don Attaque en rotation                                 | Immunité energie Positive 10%                            |Critiques massifs: 3d10             |
| Immunité sorts mentaux    |Immunité tranchant +15%    |Immunité perforant +5%             |Don Enchainement                                        | Immunité Feu 10%                            |Points de vie +50              |
| Immunité terreur          |JS universel +7            |Immunité tranchant +5%             |Don Grande poigne                                       | Immunité Magique 10%                            |Régénération Vampirique 3             |
| Immunité contondant +10%  |Points de vie +50          |Liberté de mouvement               |Don Science de l'attaque en puissance                   | Immunité perforant +5%                            |             |
| Immunité perforant +10%   |RD froid 30/-              |Points de vie +50                  |Don Spécialisation martiale (grande hache)              | Immunité Son +10%                            |             |
| Immunité tranchant +10%   |Bouclier (5)  /∞           |Rapidité                           |Don Spécialisation martiale (hache de lancer)           | Immunité Tranchant +5%                            |             |
| Points de vie +50         |                           |Régénération +10                    |Don Spécialisation martiale sup. (grande hache)         | JS universel +5                            |             |
| Cri de guerre (7) 5/J     |                           |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)| Points de vie +50                             |             |
|                           |                           |                                   |Don Spécialisation martiale sup. (hache de lancer)      | Régénération +3                            |               |
|                           |                           |                                   |Immunité contondant +5%                                 | Régénration (13) 1/J                             |            |
|                           |                           |                                   |Immunité perforant +5%                                  |                             |             |
|                           |                           |                                   |Immunité tranchant +5%                                  |                             |             |
|                           |                           |                                   |Points de vie +50                                       |                             |             |
|                           |                           |                                   |DR 8/Métal (fer)                                        |                             |             |
|                           |                           |                                   |Régénération +2                                |                             |             |
|                           |                           |                                   |Avatar des tempêtes 3/J                                 |                             |             |

# Barde ( blanc N,  Bleu N, JAUNE Partiel: manque 2 choix d'armes)

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                               | Torse (8/3)                    | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes #1 Rapière |Choix d'armes #2 |Choix d'armes #3 |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|------------|------------|
|Art de la magie +20           |Concentration 20             |CA 7 (esquive)                      |Escamotage 20  |Constitution 6|  **Rapière Dansante du Touquet**           |             |             |
| Bluff+25                   |Diplomatie 25    |Lanceur de sorts polyvaent (Barde)                  |Parade 25  |Force 8                             |Au toucher: Anathème 100%/5rds        |    |             |
| Discrétion 20             |Déplacement Silencieux 20    |Vélocité|UOM 20          |Esquive Extraordinaire                             |Parade 10     |        |             |
| Estimation 20      |Représentation 25            |Immunité Son +50%  |Dextérité 10           |Immunité Contondant 5% |Raillerie 10                             |      |         |             |
| Raillerie 25    |CA 10 (Armure)   |Liberté de mouvement  |Arme de prédilection sup (Bâton) |Immunité Electricité 15%                             |Dégâts magiques 1d6       |        |             |
| Charisme 12             |Don Artiste  |Rapidité           |Ecole renforcée (Enchantement)  |Immunité Feu 15%                             |Dégâts tranchants 1d4        |    |             |
| CA 7 (Parade)     | Immunité contondant +5%  |  Régénération 5                  |Ecole supérieure (Enchantement)  | Immunité Froid 15%                          |Altération +7           |      |             |            |
| Chants de barde supplémentaires |Immunité perforant +10%        |Cacophonie / ∞             |Don Négociation |Immunité Magique 30%                             |Critiques massifs 3d12     |           |             |
| Immunité perforant +5%      |Immunité tranchant +5%   | |Spécialisation martiale (Bâton) |Immunité Perforant 5%                            |Régénération Vampirique 1      |              |             |
|Immunité Tranchant 5%     |JS universel +7                          | |Immunité contondant +5%   |Immunité Tranchant 5%                          |Coup au but (5) 3/J           |           |              |
|Résistance Son 30/-    |Points de vie +50                           | | Immunité perforant +5%  |JS Universel +5                             |     |             |             |
|     |DR 8/Métal (Mithral)                          |  | Immunité tranchant +5% |Points de Vie +50                             |       |             |           |
|           |                          |  | Régénération 2  |Régénération 3                             | |             |             |
|  |                          |        | Sorts suppl. Barde 1 à 6 (2 fois)  |Résistance dégâts magiques 10/-                             |     |             |             |
|                           |                          |                                  | Charme monstres de groupe 3/J  |Reflets d'ombre (11) 1/J                             |             |             |             |

# Chaman

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Druide

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Éclaireur

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Élu Divin ( blanc N,  Bleu N, JAUNE Partiel: manque 2 choix d'armes)

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Art de la magie +20          |Au toucher Lumière brûlante niveau 3         |CA 7 (esquive)                     |Premiers Secours +20                                              |Don Ruine Mortelle                             |Marteau de Purification de Chesemtet             |
| Concentration +25   |CA 10 (Armure)             |Don magie de guerre                   |Charisme 12            |Immunité: Absorption de niveau/de caractéristiques                             |Marteau de Guerre             |
| Diplomatie +20             |Immunité contondant +5%      |Pierre ioun Sagesse +1 |Force 10           |Immunité: Sorts Mentaux                             |Au toucher: Brêche mineure niveau 1             |
| Sagesse 12              |Immunité perforant +5%    |Immunité Divin Bonus 30%                      |Don Amélioration de la magie curative  |Immunité contondant +5%                                |Concentration +10             |
| CA 8 (Parade)                |Immunité tranchant +5% |Immunité Postif bonus 50%            |Don Préparation de potions         |Immunité perforant +5%                             |Bonus dégâts: Divin 2d6             |
| Don Ecole renforcée (évocation) |JS universel +7      |Liberté de Mouvement           |Immunité contondant +5%                                |Immunité tranchant +5%                             |Bonus dégâts vs Extérieurs 2d6            |
| Don Ecole supérieure (évocation)    |Points de vie +50    |Rapidité           |Immunité  Négatif Bonus 30%                                      |JS universel +2                              |Bonus à l'attaque 9             |
| Don Efficacité des sorts accrue        |DR 8/Métal (Sombracier)          |Régénération +6          |Immunité perforant +5%                                      |Points de vie +25                              |Critiques Massifs 3d12             |
| Don Efficacité des sorts supérieure   |Régénération +2         |Résistance Divin 5/-             |Immunité tranchant +5%                    |Régénération +4                               |Régénération Vampirique 1             |
| Immunité contondant +5%   |Sorts supplémentaire 1 à 9                |Résistance Négatif 5/-                |Points de vie +25               |Sorts supplémentaire 1 à 9                               |             |
| Immunité perforant +5%   |         |Résistance Positif 5/-                     |Sorts supplémentaire 1 à 9        |                             |             |
| Immunité tranchant +5%          |                         |                 |Utilisation Soins Intensifs(12) /∞       |                             |             |
| Sorts supplémentaire 1 à 9    |                          |              ||                             |             |

# Ensorceleur

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |Blanc

# Guerrier ( blanc OK,  Bleu N, Jaune OK)

Blanc

| Tête                      | Torse (10/2)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |CA 8 (Armure)           |Constitution 12                      |Parade +15  |                             |             |
| Raillerie+15                  |Don Ruine artificielle            |CA 7 (esquive)                    |Force 12  |                             |             |
| CA 6 (Parade)               |Immunité Magie de Mort    |Don Initiative supérieure|Don Arme de prédilection sup. (arbalète lourde)            |                             |             |
|                                      |Immunité contondant 10%      |Don Résistance au froid    |Don Attaque en puissance |             |
| Don Borné                   |Immunité perforant +10%  | Don Science de l'initiative            |Don Désarmement  |                             |             |
| Don Combat en aveugle     |Immunité tranchant +10%   |Don Science du combat à mains nues  |Don Maniement des armes exotiques |                             |             |
| Don Esquive               |JS universel +5   |Immunité Renversement            |                                     |                             |             |
| Don Expert tacticien     |Points de vie +50            |Immunité contondant +5%             |Don Science de la défense à deux armes   |                             |             |
|                                      |Réduction dégats 6/Métal (fer)       |Immunité perforant +5%              |Don Science du combat à deux armes |                             |             |
| Don Volonté de fer        |Régénération +5  |Immunité tranchant +5% |Don Spécialisation martiale (arbalète lourde) |                             |             |
| Immunité contondant +10%   |  Résistance magie +36                 |Liberté de mouvement  |Don Spécialisation martiale sup. (arbalète lourde)                             |             |
| Immunité perforant +10%    |                          |Ponts de vie +30|Don Tir à bout portant  |                             |             |
| Immunité tranchant +10%  |                          |Rapidité |Immunité contondant +5%  |                             |             |
|                                             |                          | Régénération +2 |Immunité perforant +5%  |                             |             |
|                                             |                          | Bouclier (5) / ∞  |Immunité tranchant +5%  |                             |             |
| Héroïsme suprême 1/J  |                          |        |Points de vie +50 |                             |             |
|                           |                          |                                  |Régénération +2   |                             |             |
|                           |                          |                                  |Déclamation 3/J   |                             |             |
|                           |                          |                                  |                                      |                             |             |
|                           |                          |                                  |                               |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +25          |CA 10 (Armure)           |Constitution 12                      |Parade +25  |                             |             |
| Raillerie+25              |Don Ruine artificielle            |CA 7 (esquive)                    |Force 12  |                             |             |
| CA 8 (Parade)             |Immunité Magie de Mort    |Don Initiative supérieure|Don Arme de prédilection sup. (arbalète lourde)            |                             |             |
| Don Assaut expert         |Immunité contondant 10%      |Don Résistance au froid    |Don Attaque en puissance |             |
| Don Borné                 |Immunité perforant +10%  | Don Science de l'initiative            |Don Désarmement  |                             |             |
| Don Combat en aveugle     |Immunité tranchant +10%   |Don Science du combat à mains nues  |Don Maniement des armes exotiques |                             |             |
| Don Esquive               |JS universel +7   |Immunité Renversement            |Don Pierre d'ioun STR+1  |                             |             |
| Don Expert tacticien     |Points de vie +50            |Immunité contondant +5%             |Don Résistance au feu  |                             |             |
| Don Résistance à l'acide  |Réduction dégats 8/Métal (fer)       |Immunité perforant +5%              |Don Science de la défense à deux armes |                             |             |
| Don Volonté de fer        |Régénération +7   |Immunité tranchant +5% |Don Spécialisation martiale (arbalète lourde) |                             |             |
| Immunité contondant +10%   |  Résistance magie +40                  |Liberté de mouvement  |                             |             |
| Immunité perforant +10%    |                          |Ponts de vie +30| Don Spécialisation martiale sup. (arbalète lourde) |                             |             |
| Immunité tranchant +10%  |                          |Rapidité |Don Tir à bout portant  |                             |             |
| Points de vie +30     |                          | Régénération +2 |Immunité contondant +5%  |                             |             |
| Régénération +2           |                          | Bouclier (5) / ∞  |Immunité perforant +5%  |                             |             |
| Héroïsme suprême 2/J  |                          |        | Immunité tranchant +5%|                             |             |
|                           |                          |                                  | Points de vie +50  |                             |             |
|                           |                          |                                  | Régénération +2|                             |             |
|                           |                          |                                  | Déclamation 3/J                                      |                             |             |
|                           |                          |                                  |                               |                             |             |


 Jaune

| Tête                               | Torse (8/3)                    | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes #1 Bouclier |Choix d'armes #2 Épée à deux mains |Choix d'armes #3 |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|------------|------------|
| Intimidation +25           |CA 10 (Armure)             |Constitution 12                      |Parade +25  |Attaque sournoise 2d6|  **Pavois Lourd d'Anthurion**           |   **Flamberge Triomphante d'Anthurion**          |**Estramaçon Effroyable d'Anthurion**             |
| Raillerie+25                   |Don Ruine artificielle    |CA 7 (esquive)                    |Force 12  |Ruine naturelle                             |  CA base +4           |      BA+ 7      |Au toucher: Anathème 5% / 5rds             |
| CA 8 (Parade)                |Immunité Magie de Mort    |Don Initiative supérieure|Don Arme de prédilection sup. (arbalète lourde)            |Immunité attaques sournoises                             |       Malus à l'attaque -2      |     Critique massifs +3d12        |Intimidation 10             |
| Don Assaut expert        |Immunité contondant 10%      |Don Résistance au froid    |Don Attaque en puissance |Immunité coups critiques             |        Malus d'armure aux tests -10     |    Regénération Vampirique +3         |Contondant 2d6            |
| Don Borné                    |Immunité perforant +10%  | Don Science de l'initiative            |Don Désarmement  |Immunité acide 5%                             |   Risque d'échec des sorts profanes : 50%          |    Contondants 2d6         |Dégâts Electricité 1             |
| Don Combat en aveugle     |Immunité tranchant +10%   |Don Science du combat à mains nues  |Don Maniement des armes exotiques |Immunité contondant 5%                             |      Don requis : Maniement des pavois       |     Arme emflamée (17) / Touché      |  Altération +7             |
| Don Esquive               |JS universel +7   |Immunité Renversement            |Don Pierre d'ioun STR+1  |Immunité Feu 5%                             |     Ca Parade : 7 (Bouclier)        |     Sanglante        |Critique massifs +3d12               |
| Don Expert tacticien     |Points de vie +50            |Immunité contondant +5%             |Don Résistance au feu  |Immunité Froid 5%                             |   Immunité aux dégâts : Acide Bonus +15%          |      Points de vie  +30      | Points de vie  +10            |
| Don Résistance à l'acide  |Réduction dégats 8/Métal (fer)       |Immunité perforant +5%              |Don Science de la défense à deux armes |Immunité Perforant 5%                             |      Immunité aux dégâts : Divin Bonus +15%       |Bonus Raillerie +10            |   Régénération Vampirique 2    |
| Don Volonté de fer        |Régénération +7   |Immunité tranchant +5% |Don Science du combat à deux armes |Immunité Tranchant 5%                             |   Immunité aux dégâts :Énergie négative  Bonus +15%          |             |Avatar des Tempêtes 1/J             |
| Immunité contondant +10%   |  Résistance magie +40                  |Liberté de mouvement  | Don Spécialisation martiale (arbalète lourde)                            |JS universel +5             |     Immunité aux dégâts : Énergie positive Bonus +15%        |             |            |
| Immunité perforant +10%    |                          |Ponts de vie +30| Don Spécialisation martiale sup. (arbalète lourde)   |Points de vie +30                           |    Immunité aux dégâts : Feu Bonus +15%          |             |             |
| Immunité tranchant +10%  |                          |Rapidité | Don Tir à bout portant  |Régénération +4                             |         Immunité aux dégâts : Froid Bonus +15%        |             |             |
| Points de vie +30     |                          | Régénération +2 | Immunité contondant +5% |Utilisation régénération(13) 1/J                              |   Points de vie +30          |             |             |
| Régénération +2           |                          | Bouclier (5) / ∞  | Immunité perforant +5%  |                             |    Régénération +2          |             |             |
| Héroïsme suprême 2/J  |                          |        | Immunité tranchant +5%  |                             |     Immunité contre les énergies destructives 2/J        |             |             |
|                           |                          |                                  | Points de vie +50  |                             |             |             |             |
|                           |                          |                                  | Régénération +2  |                             |             |             |             |
|                           |                          |                                  | Déclamation 3/J  |                             |             |             |             |

# Hexblade

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |Blanc

# Magicien

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Malfrat

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Moine

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |Blanc

# Ninja

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Paladin

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Prêtre

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Rôdeur

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |Blanc

# Roublard

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Sorcier

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

# Spadassin

Blanc

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

Bleu

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |

 Jaune

| Tête                      | Torse (8/3)              | Pieds                            | Poignets                                               |Dos                                    |Choix d'armes |
| --------------------------|--------------------------|----------------------------------|--------------------------------------------------------|----------------------------------------|------------|
| Intimidation +15          |Constitution 12           |Dextérité 6                       |Force 8                                                 |                             |             |
| Perception auditive +10   |CA 8 (Armure)             |CA 5 (esquive)                    |Don Arme de prédilection sup. (grande hache)            |                             |             |
| Raillerie +15             |Pierre ioun constit +1    |Don Maniement des armes exotiques |Don Arme de prédilection sup. (hache d'arme)            |                             |             |
| CA 6 (Parade)             |Pierre ioun force +1      |Don Vélocité                      |Don Arme de prédilection sup. (hache de guerre naine)   |                             |             |
| Don Odorat                |Immunité contondant +15%  |Immunité renversement             |Don Arme de prédilection sup. (hache de lancer)         |                             |             |
| Don Rages supplémentaires |Immunité perforant +15%   |Immunité contondant +5%           |Don Attaque en rotation                                 |                             |             |
| Immunité sorts mentaux    |Immunité tranchant +15%   |Immunité perforant +5%            |Don Enchainement                                        |                             |             |
| Immunité terreur          |JS universel +5           |Immunité tranchant +5%            |Don Grande poigne                                       |                             |             |
| Immunité contondant +10%  |Points de vie +50         |Liberté de mouvement              |Don Science de l'attaque en puissance                   |                             |             |
| Immunité perforant +10%   |RD froid 10/-             |Points de vie +50                 |Don Spécialisation martiale (grande hache)              |                             |             |
| Immunité tranchant +10%   |Bouclier (5) 5/J          |Rapidité                          |Don Spécialisation martiale (hache de lancer)           |                             |             |
| Points de vie +50         |                          |Régénération +6                   |Don Spécialisation martiale sup. (grande hache)         |                             |             |
| Cri de guerre (7) 5/J     |                          |Héroisme suprême 1/J              |Don Spécialisation martiale sup. (hache de guerre naine)|                             |             |
|                           |                          |                                  |Don Spécialisation martiale sup. (hache de lancer)      |                             |             |
|                           |                          |                                  |Immunité contondant +5%                                 |                             |             |
|                           |                          |                                  |Immunité perforant +5%                                  |                             |             |
|                           |                          |                                  |Immunité tranchant +5%                                  |                             |             |
|                           |                          |                                  |Points de vie +50                                       |                             |             |
|                           |                          |                                  |DR 6/Métal (fer)                                        |                             |             |
|                           |                          |                                  |Avatar des tempêtes 1/J                                 |                             |             |