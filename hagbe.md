<!-- TITLE: Hagbe -->
<!-- SUBTITLE:  -->

![Hagbe](/uploads/alricqg-6-140738-d.jpg "Alricqg 6 140738 D")

> Oyez Oyez Aquafondais !
> 
> Je me présente, Maitre Hagbe. J'ai décidé de m'exiler loin des tumultes de la ville ([Explication détaillée](http://lacoleredaurile.xooit.fr/t1377-Une-nouvelle-race-de-cr-ature.htm)). Mon disciple Brokk reprend le flambeau et devient ainsi le nouveau forgeron officiel d'Eauprofonde. Aussi si vous souhaitez à tout prix me voir, je suis parti sur une ile tropicale. Voyez Brokk pour plus d'informations.
> 
> 
> Pour ceux qui viendrait me voir coute que coute, je suis capable d'améliorer de façon permanente vos objets même magiques. Ceux ayant déjà le même pouvoir se CUMULERONT à mes améliorations proposées. Toutefois je ne ferai qu'une SEULE et UNIQUE amélioration par objet.


# Accès

# Achats / Ventes

# Enchantements

## Armes à distance

| Propriété magique        | Prix (po) |
| ------------------------ | --------: |
| Arme de force +5         | 100 000   |
| Arme de force +10        | 300 000   |
| Arme de force +15        | 400 000   |
| Arme de force +20        | 500 000   |
| Munitions illimitées     | 200 000   |
| 1d6 acide                | 300 000   |
| 1d6 froid                | 300 000   |
| 1d6 feu                  | 300 000   |
| 1d6 son                  | 300 000   |
| 1d6 énergie positive     | 300 000   |
| 1d6 énergie négative     | 300 000   |
| Rapidité                 | 300 000   |
| Résistance au sorts +10  | 150 000   |
| Régénération +2          | 450 000   |
| Bonus altération +1      | 150 000   |
| Bonus altération +2      | 450 000   |
| Bonus attaque +1         | 100 000   |
| Bonus attaque +2         | 200 000   |
| Bonus attaque +4         | 450 000   |

## Armes de corps à corps

| Propriété magique        | Prix (po) |
| ------------------------ | --------: |
| 1d6 acide                | 300 000   |
| 1d6 froid                | 300 000   |
| 1d6 feu                  | 300 000   |
| 1d6 son                  | 300 000   |
| 1d6 énergie positive     | 300 000   |
| 1d6 énergie négative     | 300 000   |
| Rapidité                 | 300 000   |
| Acérée                   | 300 000   |
| Résistance au sorts +10  | 150 000   |
| Régénération +2          | 450 000   |
| Bonus altération +1      | 150 000   |
| Bonus altération +2      | 450 000   |
| Bonus attaque +1         | 100 000   |
| Bonus attaque +2         | 200 000   |
| Bonus attaque +4         | 450 000   |


## Boucliers

| Propriété magique                    | Prix (po) |
| ------------------------------------ | --------: |
| Régénération +1                      | 150 000   |
| Résistance aux sorts +10             | 150 000   |
| Bonus jet de sauvegarde vigueur +7   | 150 000   |
| Bonus jet de sauvegarde réflexes +7  | 150 000   |
| Bonus jet de sauvegarde volonté +7   | 150 000   |
| Régénération +2                      | 450 000   |
| Force +2                             | 450 000   |
| Dextérité +2                         | 450 000   |
| Constitution +2                      | 450 000   |
| Sagesse +2                           | 450 000   |
| Intelligence +2                      | 450 000   |
| Charisme +2                          | 450 000   |


## Armures

| Propriété magique     | Prix (po) |
| --------------------- | --------: |
| Bonus armure +2       | 450 000   |
| Force +2              | 450 000   |
| Dextérité +2          | 450 000   |
| Constitution +2       | 450 000   |
| Sagesse +2            | 450 000   |
| Intelligence +2       | 450 000   |
| Charisme +2           | 450 000   |
| Régénération +2       | 450 000   |
| Liberté de mouvement  | 300 000   |

## Casques
| Propriété magique                             | Prix (po) |
| --------------------------------------------- | --------: |
| Résistance aux dégats d'acide +5/-            | 300 000   |
| Résistance aux dégats de son +5/-             | 300 000   |
| Résistance aux dégats froid +5/-              | 300 000   |
| Résistance aux dégats d'énergie divine +5/-   | 300 000   |
| Résistance aux dégats d'électricité +5/-      | 300 000   |
| Résistance aux dégats de feu +5/-             | 300 000   |
| Résistance aux dégats magiques +5/-           | 300 000   |
| Résistance aux dégats d'énergie positive +5/- | 300 000   |
| Résistance aux dégats d'énergie négative +5/- | 300 000   |
| Régénération +2                               | 450 000   |
| Force +2                                      | 450 000   |
| Dextérité +2                                  | 450 000   |
| Constitution +2                               | 450 000   |
| Sagesse +2                                    | 450 000   |
| Intelligence +2                               | 450 000   |
| Charisme +2                                   | 450 000   |

## Capes

| Propriété magique  | Prix (po) |
| ------------------ | --------: |
| Bonus d'armure +2  | 300 000   |
| Régénération +2    | 450 000   |
| Force +2           | 450 000   |
| Dextérité +2       | 450 000   |
| Constitution +2    | 450 000   |
| Sagesse +2         | 450 000   |
| Intelligence +2    | 450 000   |
| Charisme +2        | 450 000   |
| Rapidité           | 300 000   |

## Ceintures

| Propriété magique  | Prix (po) |
| ------------------ | --------: |
| Régénération +2    | 450 000   |
| Force +2           | 450 000   |
| Dextérité +2       | 450 000   |
| Constitution +2    | 450 000   |
| Sagesse +2         | 450 000   |
| Intelligence +2    | 450 000   |
| Charisme +2        | 450 000   |


## Bottes

| Propriété magique   | Prix (po) |
| ------------------- | --------: |
| Régénération +1     | 150 000   |
| Vision dans le noir | 75 000    |
| Régénération +2     | 450 000   |
| Force +2            | 450 000   |
| Dextérité +2        | 450 000   |
| Constitution +2     | 450 000   |
| Sagesse +2          | 450 000   |
| Intelligence +2     | 450 000   |
| Charisme +2         | 450 000   |
| Rapidité            | 300 000   |

## Bracelets / Gantelets

| Propriété magique       | Prix (po) |
| ----------------------- | --------: |
| Régénération +2         | 450 000   |
| Force +2                | 450 000   |
| Dextérité +2            | 450 000   |
| Constitution +2         | 450 000   |
| Sagesse +2              | 450 000   |
| Intelligence +2         | 450 000   |
| Charisme +2             | 450 000   |
| 1d6 acide               | 300 000   |
| 1d6 froid ¹             | 300 000   |
| 1d6 feu ¹               | 300 000   |
| 1d6 énergie positive ¹  | 300 000   |
| 1d6 énergie négative ¹  | 300 000   |
| Rapidité                | 300 000   |
| Résistance au sorts +10 | 150 000   |
| Bonus altération +1 ¹   | 150 000   |
| Bonus attaque +1 ¹      | 100 000   |
| Bonus attaque +2 ¹      | 200 000   |
| Bonus attaque +4 ¹      | 450 000   |
| Bonus altération +2 ¹   | 450 000   |
| 1d6 son ¹               | 300 000   |

- ¹ : Gantelets uniquement


## Anneaux

| Propriété magique                                | Prix (po) |
| ------------------------------------------------ | --------: |
| Liberté de mouvement                             | 300 000   |
| Immunité à la terreur                            | 150 000   |
| Immunité à l'absorption caractéristiques/niveaux | 300 000   |
| Régénération +2                                  | 450 000   |
| Force +2                                         | 450 000   |
| Dextérité +2                                     | 450 000   |
| Constitution +2                                  | 450 000   |
| Sagesse +2                                       | 450 000   |
| Intelligence +2                                  | 450 000   |
| Charisme +2                                      | 450 000   |

## Amulettes
| Propriété magique                | Prix (po) |
| -------------------------------- | --------: |
| Acrobatie +15                    | 350 000   |
| Bluff +15                        | 350 000   |
| Concentration +15                | 350 000   |
| Art de la magie +15              | 350 000   |
| Création d'arme +15              | 350 000   |
| Création d'armure +15            | 350 000   |
| Crochetage +15                   | 350 000   |
| Désamorçage/Sabotage +15         | 350 000   |
| Détection +15                    | 350 000   |
| Déplacement silencieux +15       | 350 000   |
| Discrétion +15                   | 350 000   |
| Diplomatie +15                   | 350 000   |
| Estimation +15                   | 350 000   |
| Fabrication pièges +15           | 350 000   |
| Fouille +15                      | 350 000   |
| Intimidation +15                 | 350 000   |
| Parade +15                       | 350 000   |
| Perception auditive +15          | 350 000   |
| Pose de piège +15                | 350 000   |
| Premiers secours +15             | 350 000   |
| Raillerie +15                    | 350 000   |
| Représentation +15               | 350 000   |
| Utilisation objets magiques +15  | 350 000   |
| Escamotage +15                   | 350 000   |
| Survie +15                       | 350 000   |
| Alchimie +15                     | 350 000   |
| Savoir +15                       | 350 000   |
| Régénération +2                  | 450 000   |
| Force +2                         | 450 000   |
| Dextérité +2                     | 450 000   |
| Constitution +2                  | 450 000   |
| Sagesse +2                       | 450 000   |
| Intelligence +2                  | 450 000   |
| Charisme +2                      | 450 000   |
