<!-- TITLE: (Niv 06) Eauprofonde - Le Temple Du Losange -->
<!-- SUBTITLE: Le Culte du Losange -->

# Résumé
Situé dans les profondeurs de la ville, ce lieux secret est le repaire du culte du Losange, une secte pratiquant les sciences occultes. On ne sait que très peu de chose sur celle-ci, mais les autorités d'Eauprofonde suspectent cette secte dans les disparitions mystérieuses d'enfants Eauprofondais.