<!-- TITLE: (Niv 28) Iles Des Glaces - Labyrinthe -->
<!-- SUBTITLE: Le golem invincible ! -->

# Résumé
Inutile de cherche à le tuer, vous n'y arriverez pas. Votre seule chance est de trouver l'enclume magique qui pourrait le détruire. Problème : Ce donjon est parsemé de portails aléatoires qui menent à des pièces qui se ressemblent toutes ou presque. Votre survie résidera certainement dans la vitesse ou la chance ! Mais ne trainez pas, car le Golem Invicible vous traque !