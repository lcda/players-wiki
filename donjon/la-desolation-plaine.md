<!-- TITLE: (Niv 15) La Désolation - Plaine -->
<!-- SUBTITLE: Léviathan, le dragon sirène -->

# Résumé
Proches des rochers et du bord de mer se trouve Léviathan, un dragon vert. Avant d'arriver jusqu'à lui, vous serez confronter à des monstres de vases, des Sirènes des récifs, des Guenaudes naufrageuses et des Rusalka vengeresse.