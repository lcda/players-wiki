<!-- TITLE: (Niv 28-30) Ile Des Saisons - Temple Des Saisons -->
<!-- SUBTITLE: Un demi-dieu pour boss ! -->

# Résumé
Le Temple des Saisons est sur deux niveaux. L'Avatar d'Ishtar y demeure. Parsemé d'embuches, d'enigmes, et de mini-boss, ce donjon vous fera transpirez. De plus l'ile sur lequel il est, recele de nombreux secrets. Reste à les découvrir !