<!-- TITLE: (Niv 16) Epine Dorsale - Galeries -->
<!-- SUBTITLE: Karhk, le chef ogre -->

# Résumé
Certainement la plus grosse armée des environs, Karhk dirige son clan d'une main de fer. Il est aussi le gardien d'une des entrées vers Ombreterre. Les relations entre Ycingard et Karhk sont fragiles. Cela permet à chacun d'y trouver son compte : l'un garde sa route principale sauf, et l'autre évite une guerre ouverte avec Ycingard.