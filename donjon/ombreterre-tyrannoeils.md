<!-- TITLE: (Niv 30) Ombreterre - Tyrannoeils -->
<!-- SUBTITLE: N'oubliez pas votre spray au poivre-->

![Beholder](/uploads/beholder.png "Beholder"){.align-center}

# Intro

Les Tyrannoeils constituent une des factions les plus dangereuses et imprévisibles d'Ombreterre.

__Donjon de groupe, pour niveaux 25-30__

__Le donjon est disponible en plusieurs difficultés: Normal, Dificile, Epique__


[<img src="/uploads/screenshots/tyra-0.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/tyra-0.jpg)


# Quêtes associées

- (29) L'ennemi de mon ennemi ... 



# Spécificités

## Malus de zone

Des malus sont appliqués à chaque créature non hostile (joueurs, invocations, ...) dans la zone. Ces malus sont:
- -16 CA
- -12 JS Volonté
- -20 aux dégâts (infligés par des armes)
- 80% d’échec des sorts
- -48% de vitesse de déplacement

Ces malus diminuent à chaque fois que l'un des quatre grands Tyrannoeils meurt.

Les malus sont augmentés en fonction de la difficulté choisie a l'entrée du donjon.


## Combats

### Cône d'Antimagie

Tous les tyrannoeils à l'exception des magiciens émettent un cône d'antimagie de 30° et 30m de long orienté selon leur oeil principal. Celui ci dissipe tous les sorts dans la zone et empêchent de lancer des sorts (100% d’échec des sorts)

### Rayons

Chaque round, un tyrannoeil lance une série de rayons magiques aléatoirement sur les cibles proches. Le nombre de rayons, leur portée ainsi que le DD du jet de sauvegarde associé varie en fonction du type du tyrannoeil.

Les effets sont:
- Blessure
- Charme personne
- Désintégration
- Doigt de mort
- Lenteur
- Pétrification
- Sommeil
- Peur


### Magiciens

Les magiciens tyrannoeil se sont crevé les yeux afin de ne pas émettre de cône d'antimagie et de pouvoir lancer des sorts.


## Boss
Le donjon est gardé par la Mère de ruche.

Note: Il n'est pas nécessaire de tuer les grands tyrannoeils pour combattre la mère de ruche.

### Capacités spéciales

- Téléportation sur un point en hauteur de la salle
- Avale un joueur proche. Il sera téléporté dans le ventre de la mère de ruche pour y être digéré, à moins que...
- Montée de lave dans la pièce. Toute créature touchant la lave souffrira de lourds dégâts. Des explosions apparaissent quelques secondes avant que la lave arrive.