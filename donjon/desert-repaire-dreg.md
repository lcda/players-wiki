<!-- TITLE: (Niv 26) Desert - Repaire Dreg -->
<!-- SUBTITLE: Tel est pillé qui croyait piller -->
# Intro
Ce repaire sous-terrain abrite une colonie de pillards aux moeurs et aux cultes étranges.

**Donjon de groupe, pour niveau 23-28**


# Quête associée
* (26) Quelques semaines de répit

# Spécificités
Les D'reg vont parfois arriver par vagues ou rester embusqués pour vous prendre au piège , soyez vigilents.
