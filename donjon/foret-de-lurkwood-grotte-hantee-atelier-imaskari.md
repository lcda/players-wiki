<!-- TITLE: (Niv 26-28) Forêt De Lurkwood - Grotte Hantée - Atelier Imaskari -->
<!-- SUBTITLE: La Chose de l'Outremonde et Le Golem Geant Imaskari -->

# Résumé
Ce lieux est l'endroit des golems Imaskari. A leur tête un Golem Géant Imaskari qui héberge également un autre boss, la Chose de l'Outremonde. Il semblerait que le campement elfique dans la foret de lurkwood soit au courant de leur existence...