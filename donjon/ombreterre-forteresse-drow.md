<!-- TITLE: (Niv 18) Ombreterre - Forteresse Drow -->
<!-- SUBTITLE: Le boss, c'est la mère noire... -->

# Intro

Cette grande forteresse a été bâtie les Drows de la cité de Ched Nasad, et leur sert d'avant poste sécurisé pour commercer avec les différentes factions de cette région d'Ombreterre.

__Donjon de groupe, pour niveaux 20__


[<img src="/uploads/screenshots/drow-0.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/drow-0.jpg)
[<img src="/uploads/screenshots/drow-1.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/drow-1.jpg)
[<img src="/uploads/screenshots/drow-2.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/drow-2.jpg)

# Quêtes associées
* (29) L'ennemi de mon ennemi ...
# Spécificités
## Monstres
La plupart des monstres réapparaissent automatiquement si ils sont tués.

## Catapultes
Il y a plusieurs catapultes qui tirent automatiquement sur les joueurs. Il est possible de s'en approcher pour en prendre le contrôle, et les utiliser contre les drows (Un clic sur la catapulte déclenche un tir sur la créature sélectionnée par le joueur via clic droit).

## Rencontres
Il y a plusieurs rencontres sur le chemin, qui déclenchent des sorts spécifiques:
- Blocage des portes avec un champ de force: Il faut attendre
- Verrouillage des portes: Possibilité de forcer la porte
- Glyphes de garde: Dégâts magiques, à éviter
- Toiles d'araignées: Ralentissement

# Conseils
- Partir avec un groupe bien constitué (tank, healer, dps).
- Pouvoir lancer des sorts de résurrection (≠ rappel à la vie), car les tireurs drow attaqueront certainement les joueurs avec 1 pv.
- Rester groupés, certaines portes peuvent séparer le groupe
- Toujours avancer, en évitant de perdre trop de temps sur des monstres qui réapparaissent.
- Se munir d'objets, potions, parchemins ou sorts permettant d'éviter les entraves
