<!-- TITLE: (Niv 27-28) Ombreterre - Illithids -->
<!-- SUBTITLE: Regardez mes légumes ! Voyez comme ils sont dociles ! -->

# Intro

L'enclave des Illithids est le centre d'un vaste réseau de commerce d'esclaves en Ombreterre, qui s'étendrais même en surface selon certaines légendes...

L'entrée principale est en Ombreterre, mais on raconte qu'il y aurait un passage caché au marais du lézard.

__Donjon de groupe, pour niveaux 20-35__


# Quetes associées
* (29) L'ennemi de mon ennemi ...
* (25) Des gardes qui disparaissent

# Spécificités
* Certaines portes nécessitent un test de volonté pour s'ouvrir.
* Les illithid sont de grands lanceurs de sorts

# Conseils

