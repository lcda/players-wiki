<!-- TITLE: (Niv 28) Ile Des Glaces - Morts Vivants -->
<!-- SUBTITLE: Dehors il fait froid, dedans c'est chaud -->

# Intro

Les morts vivants très présents dans toute la partie haute de l'Île des Glaces semble venir d'une caverne sur le flanc nord.

__Donjon de groupe, pour niveaux 25-30__


# Quêtes associées
* (27) Des morts et une ile ...

# Spécificités
* Préparez vous à voyager dans les plans

# Conseils
