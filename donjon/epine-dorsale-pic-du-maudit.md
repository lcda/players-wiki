<!-- TITLE: (Niv 28-30) Epine Dorsale - Pic Du Maudit -->
<!-- SUBTITLE: Le jeu de l'Oie -->

# Résumé
Ce pic est inacessible par les moyens traditionels. Seul le sort "Voyage" permettra de transporter un petit groupe au pied du Pic. A son sommet, se trouve le Maudit et le dragon Tiamat !  Véritable jeu de l'oie, l'ascension sera votre principale difficulté. Munissez vous des outils d'escalade et priez pour ne pas tomber !