<!-- TITLE: (Niv 25) Ombreterre - Profondorn -->
<!-- SUBTITLE: -->

![Lcda Dorn](/uploads/lcda-dorn.jpg "Lcda Dorn"){.align-center}

# Intro

Le bastion de Profondorn appartenait autrefois à un puissant clan nain, mais leur richesse et leur savoir-faire a attiré des ennemis beaucoup trop puissants pour eux...

__Donjon de groupe, pour niveaux 20-25__


[<img src="/uploads/screenshots/dorn-0.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-0.jpg)
[<img src="/uploads/screenshots/dorn-1.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-1.jpg)
[<img src="/uploads/screenshots/dorn-2.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-2.jpg)
[<img src="/uploads/screenshots/dorn-3.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-3.jpg)
[<img src="/uploads/screenshots/dorn-4.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-4.jpg)
[<img src="/uploads/screenshots/dorn-5.jpg" style="height: 150px; display: inline-block">](/uploads/screenshots/dorn-5.jpg)


# Quêtes associées
* (26) Profondorn
# Spécificités
## Narration
Un accent a été mis sur la narration, ainsi que l'ambiance sonore du donjon. Les monstres ne réapparaissent pas, vous pouvez donc prendre votre temps.

## Objets cachés
Il y a pas mal de coffres cachés, qui vous aideront tout au mong de la progression. Gardez l'oeil ouvert !

## Pièges
Attention où vous mettez les pieds ! La plupart des pièges sont légèrement visibles sans talents de roublard, et peuvent être évités.

## Enigmes
Plusieurs énigmes, plus ou moins dificiles... Généralement les indices sont disséminés dans l'ensemble du donjon.

## Combats & repos
Certains monstres disposent d'une attaque spéciale, signalée par une marque rouge au sol, qu'il faudra éviter.

Les repos sont restreints dans le donjon, et il faudra trouver des objets spécifiques autorisant quelques repos.

## Fins
Plusieurs fins sont disponibles, suivant vos choix lors de votre parcours dans le donjon. Il est possible de refaire le donjon avec un même personnage et de découvrir les fins alternatives.


# Bestiaire

## Protecteur Nain
Guerrier nain disposant de beaucoup de points de vie.

Il dispose d'une attaque spéciale consistant en un coup de bouclier provoquant des dégâts tout autour de lui.

## Garde Royal
Version plus puissante du protecteur nain

## Prêtre de Moradin

## Arbaletrier Nain
Artilleur solide.
Il dispose d'une attaque spéciale de tir en rafale blessant par 3 fois quiconque se trouve dans l'axe de tir.