<!-- TITLE: (Niv 12) Front De Mer - Galeries -->
<!-- SUBTITLE: Golgolth et ses élementaires d'eau -->

# Résumé
Ses galeries, proches de la plage de la route cotière, hébergent une horde de creatures des eaux avec à leur tête, Golgoth, un puissant élémentaire d'eau d'environ 6 mêtre de haut