<!-- TITLE: (Niv 23-25) Front De Mer - Crique du pendu - Repaire Secret -->
<!-- SUBTITLE: Le celèbre pirate Barba Rossa -->

# Résumé
Sa tête est mise à prix depuis de longues années, Barba Rossa s'en est toujours sorti ! Son repaire n'est connu que de quelques initiés. Accompagné en permanence de son fidèle lieutenant, Mouche, il parcours les mers en pillant les bateaux de commerce en provenance d'Eauprofonde. Son armée est quasiment aussi grande que celle d'Eauprofonde. Ne vous laissez pas berner par ses beaux discours !
On raconte que Barba Rossa n'est qu'un nom, et que sa mort n'empèche jamais son remplacement dans l'heure !
