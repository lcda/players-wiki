<!-- TITLE: (Niv 09-10) Fortin Orc -->
<!-- SUBTITLE: Le chef Grammsh ! -->

# Résumé
Ce fortin est situé à l'orée de la forêt de Lurkwood. C'est une véritable forteresse. Proche de la grande route de commerce reliant Ycingard à Eauprofonde, ils font parfois quelques pillages sur les caravanes marchandes.