<!-- TITLE: (Niv 17) Epine Dorsale - Nid De Wyverns -->
<!-- SUBTITLE: Une famille de Wyverns -->

# Résumé
Dans les hauteurs de l'épine dorsale, vit une famille nombreuse de Wyverns où leur nid se trouve dans une grande grotte. Outre sa dangerosité naturelle, prenez garde à leurs queues empoisonnées et ses dards qui peuvent vous tuer d'un coup ! 