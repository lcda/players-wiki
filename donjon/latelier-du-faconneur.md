<!-- TITLE: (Niv 29) L'atelier Du Façonneur -->
<!-- SUBTITLE: Le Golem Polymorphe -->

# Résumé
Cet endroit secret est très difficile d'accès. Une personne aurait crée ce lieux ainsi que ces créatures. Personne ne le connait mais tous l'appellent le Façonneur. Sa plus grande réalisation est un golem polymorphe capable de se transformer toutes les minutes.