<!-- TITLE: (Niv 30) Désert - Le Dédale -->
<!-- SUBTITLE: Le Ravageur Elémentaire -->

# Résumé
Ce donjon est parmi les plus hardcore du serveur car il ne permet aucune erreure d'organisation. Pour autant il n'est pas plus dur que les Géants. La difficulté réside dans les effets de zones où l'aventurier parcourera à la fois une zone de feu, d'air, de terre et d'eau avant d'atteindre la salle du boss. Amateur s'abstenir ! Une petite équipe bien organisée sera bien plus efficace qu'un gros groupe pour ce donjon.