<!-- TITLE: (Niv 01-03) Eauprofonde - Egouts -->
<!-- SUBTITLE: Dératisation de rats et premier boss -->

# Résumé
Il semblerait que les rats vivants dans les égouts ont subi une mutation à cause d'une source plus que douteuse. Nul ne sait comment cela est arrivé, mais votre mission est de dératiser ces égouts afin d'éviter une invasion de rats mutants dans la ville. Vous devrez affronter le boss de ce lieux, un rat mutant boosté aux hormones ! Traquez le et tuez le ! Mais n'oubliez pas de dépolluer la source infectieuse ! 